This module adds [highlight](https://ckeditor.com/docs/ckeditor5/latest/features/highlight.html)
feature to CKEditor 5.

    The highlight feature offers text marking tools that help content authors
    speed up their work, for example when reviewing content or marking it for
    future reference. It uses inline <mark> elements in the view, supports both
    markers (background color) and pens (text color), and comes with a flexible
    configuration.
